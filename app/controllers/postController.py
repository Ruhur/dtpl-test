from app import app
from flask_login import current_user
from flask import redirect, render_template, request, url_for
from app.models.DB.User import getUserIdByEmail
from app.models.Post import savePost

@app.route('/createPost', methods=["POST"])
def createPost():
    if current_user.is_authenticated and request.form.get('post'):
        user = getUserIdByEmail(current_user.id)
        # create post
        save = savePost(request.form.get('post'), request.form.get('idKomunitas'), user['id'])
        # create post komunitas
        # redirect
        return redirect(url_for('memberViewCommunity', id=request.form.get('idKomunitas')))
    return redirect(url_for('memberViewCommunity', id=request.form.get('idKomunitas'))) 
    

from app import app
from flask_login import current_user
from flask import redirect, render_template, request, url_for
from app.models.Komunitas import getKomunitasByName, getDetailKomunitas, isMember, getMembers
from app.models.Post import getAllPost
from app.models.DB.User import getUserIdByEmail

# from app.models.user import User ## import kelas User dari model


@app.route('/memberHome')
def memberHome():
    # user =  User() ## membuat objek dari kelas user
    # nama = user.getName() ## memanggil method untuk mengambil nama
    return render_template('member/home.html')


@app.route('/memberMyProfile')
def memberMyProfile():
    return render_template('member/myProfile.html')


@app.route('/memberViewCommunity/<id>')
def memberViewCommunity(id):
    if(id != None and current_user.is_authenticated):
        detail = getDetailKomunitas(id)
        idUser = getUserIdByEmail(current_user.id)
        isaMember = isMember(idUser['id'], id)
        # return render_template('member/viewCommunity.html', komunitas=detail, isaMember=isaMember)
        posts = getAllPost(id)
        members = getMembers(id) 
        return render_template('member/viewCommunity.html', komunitas=detail, isaMember=isaMember, posts=posts, members=members )
    return redirect(url_for('login'))


@app.route('/dashboard/home')
def dashboardHome():
    komunitas = ""
    if(request.args.get('name') != ''):
        komunitas = getKomunitasByName(request.args.get('name'))
    print(komunitas)
    return render_template('member/dashboard.html', komunitas=komunitas)

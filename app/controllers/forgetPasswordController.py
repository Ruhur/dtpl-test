from app import app
from app.models.DB.User import getUserByEmail, isUserExist, updatePassword
from app.helper import helper
from flask import render_template, request, redirect, url_for, session


@app.route('/forgetPassword', methods=["POST", "GET"])
def forgetPassword():
    if request.method == "POST":
        email = request.form["email"]
        user = getUserByEmail(email)
        if not user:
            helper.log_info('Wrong email!')
            return render_template('forgetPassword.html')
        else:
            session["email"] = email
            return redirect(url_for('newPassword'))

    else:
        return render_template('forgetPassword.html')


@app.route('/newPassword', methods=["POST", "GET"])
def newPassword():
    email = session.get("email")
    if not email:
        return redirect(url_for('forgetPassword'))
    if request.method == "POST":
        password1 = request.form["password1"]
        password2 = request.form["password2"]
        if not isUserExist(email):
            helper.log_info("Email is not exist")
            return render_template("newPassword.html")
        if (password1 != password2):
            helper.log_info("Password is not match, please try again!")
            return render_template("newPassword.html")
        else:
            if updatePassword(email, password1):
                session.pop('email', None)
                return redirect(url_for('forgetPasswordSuccess'))
            else:
                helper.log_info("Something went wrong :(")
                return render_template('newPassword.html')
    else:
        return render_template('newPassword.html')


@app.route('/forgetPasswordSuccess', methods=["GET"])
def forgetPasswordSuccess():
    return render_template('forgetPasswordSuccess.html'), {"Refresh": "3; url=/"}

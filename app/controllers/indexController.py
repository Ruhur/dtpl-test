
from app import app
from app.models.Komunitas import getKomunitasByName
from flask import render_template, request, jsonify, session
from flask_login import LoginManager, current_user, login_user, logout_user, user_logged_in


# from app.models.user import User ## import kelas User dari model


@app.route('/', methods=['GET'])
def index():
    # user =  User() ## membuat objek dari kelas user
    # nama = user.getName() ## memanggil method untuk mengambil nama
    if(request.args.get('name') != ''):
        komunitas = getKomunitasByName(request.args.get('name'))
        print("--------------------------------------------------")
        print(current_user.is_authenticated)

        # if(user.get_role() == "admin"):
        #     return render_template('admin/home.html')
        # elif(user.get_role() == "member"):

        #     # return render_template('test.html', id_asd=request.args.get('id'), komunitas=komunitas)
        #     return redirect(url_for('member/dashboard.html', name=user.get_name()))
        # elif(user.get_role() == "community"):
        #     return render_template('community/home.html')
        # else:
        #     return redirect(url_for('index'))

        return render_template('index.html', komunitas=komunitas)
    return render_template('index.html')


@app.route('/test', methods=['GET'])
def test():
    print(request.args.get('name'))
    komunitas = ''
    if(request.args.get('name') != ''):
        komunitas = getKomunitasByName(request.args.get('name'))
    return render_template('test.html', id_asd=request.args.get('id'), komunitas=komunitas)

from app import app
from flask import render_template
# from app.models.user import User ## import kelas User dari model

@app.route('/joinus')
def joinus():
	# user =  User() ## membuat objek dari kelas user
	# nama = user.getName() ## memanggil method untuk mengambil nama
	return render_template('joinus.html')
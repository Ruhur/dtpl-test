from datetime import timedelta
from this import d

from flask_login import LoginManager, current_user, login_user, logout_user, user_logged_in
from app import app
from app.models.DB.User import getUserByEmail, getUserIdByEmail
from app.helper import helper
from app.models.Komunitas import getKomunitasByName, getIdKomunitasByIdUser
from flask import render_template, request, redirect, url_for, Flask, session, flash
# from app.models.user import User ## import kelas User dari model

lm = LoginManager()

# <SET SESSION LIFETIME>


# this section overwrite with <session.permanent = True> statement in code find below
# app.permanent_session_lifetime = timedelta(minutes == 12)

app.secret_key = "hello"

# buat compare hash
# from hmac import compare_digest
# >>> def verify(cookie, sig):
# ...     good_sig = sign(cookie)
# ...     return compare_digest(good_sig, sig)


@app.route('/login', methods=["POST", "GET"])
def login():
    if request.method == "POST":
        email = request.form["email"]
        password = request.form["password"]
        user = getUserByEmail(email)

        if not user:
            helper.log_info('Wrong email!')
            return render_template('login.html')

        if user.check_password(password):
            helper.log_debug('Success check password: '+user.get_name())
            login_user(user)

            session['name'] = user.get_name()
            session['role'] = user.get_role()

            if(user.get_role() == "admin"):
                return render_template('admin/home.html')
            elif(user.get_role() == "member"):

                # return render_template('test.html', id_asd=request.args.get('id'), komunitas=komunitas)
                print("ke member =====================================================")
                return render_template('member/dashboard.html', name=user.get_name())
            elif(user.get_role() == "community"):
                idUser = getUserIdByEmail(email)
                idKomunitasUser = getIdKomunitasByIdUser(idUser['id'])
                print(idKomunitasUser, 'assd')
                return redirect(url_for('memberViewCommunity',  id=idKomunitasUser['id_komunitas']))  
            else:
                return redirect(url_for('index'))
        else:
            helper.log_info('Wrong password!')
            return render_template('login.html')

    else:
        if current_user.is_authenticated:
            helper.log_debug('Currently have session: ' +
                             current_user.get_name())
            return redirect(url_for('index'))

        helper.log_debug('No session found')
        return render_template('login.html')

    # return f"<h1>assalamualaikum</h1>"


@ app.route('/user')
def user():
    if current_user.is_authenticated:
        role = current_user.get_role()
        helper.log_debug('Log in as: '+role)
        if(role == "admin"):
            return render_template('admin/home.html')
        elif(role == "member"):
            return render_template('member/home.html')
        elif(role == "community"):
            return render_template('community/home.html')
        else:
            return render_template('index.html')
    else:
        return redirect(url_for('login'))


@ app.route('/logout')
def logout():
    logout_user()
    helper.log_debug('You have been logged out!')
    return redirect(url_for('index'))

from app import app
from flask import render_template
# from app.models.user import User ## import kelas User dari model


@app.route('/payment')
def payment():
    # user =  User() ## membuat objek dari kelas user
    # nama = user.getName() ## memanggil method untuk mengambil nama
    return render_template('payment.html')


@app.route('/paymentConfirmation')
def paymentConfirmation():
    # user =  User() ## membuat objek dari kelas user
    # nama = user.getName() ## memanggil method untuk mengambil nama
    return render_template('paymentConfirmation.html')

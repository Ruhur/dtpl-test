from typing import Type
from flask_login import current_user
from app import app
from flask import render_template, request, redirect, url_for, flash
from app.models.Komunitas import getDetailKomunitas, isMember, joinKomunitas as join, updateKomunitas, getMembers
from app.models.DB.User import getUserIdByEmail

@app.route('/komunitas/join', methods=["POST"])
def joinKomunitas():
    print(request.form.get('id_komunitas'))
    if current_user.is_authenticated and request.form.get('id_komunitas'):
        idKomunitas = request.form.get('id_komunitas')
        user = getUserIdByEmail(current_user.id)
        joinkom = join(user['id'], idKomunitas)
        if joinkom == 'created':
            flash("Berhasil join komunitas")
            return redirect(url_for('memberViewCommunity', id=idKomunitas)) 
        if joinkom == 'fail':
            flash("Gagal join komunitas")
            return redirect(url_for('memberViewCommunity', id=idKomunitas)) 
    return redirect(url_for('memberViewCommunity', id=idKomunitas)) 

@app.route('/komunitas/ubahDeskripsi', methods=["POST"])
def ubahDeskripsi():
    if current_user.is_authenticated and request.form.get('description') and request.form.get('idKomunitas') :
        print(request.form.get('description'), request.form.get('idKomunitas'))
        update = updateKomunitas(request.form.get('idKomunitas'), request.form.get('description'))
        return redirect(url_for('memberViewCommunity', id=request.form.get('idKomunitas'))) 
    return redirect(url_for('index'))

@app.route('/listMember/<id>')
def getAllMemberInKomunitas(id):
    if(id != None and current_user.is_authenticated):
        members = getMembers(id)
        # return
        print(members)
        pass

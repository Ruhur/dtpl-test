from app import app
from flask import render_template, request, redirect, url_for, session
import app.models.PaymentType as PT
from app.helper import helper


@app.route('/membership')
def membership():
    if request.method == "POST":
        type = request.form["type"]

        session["membershipType"] = type

        return redirect(url_for('membership'))
        # return render_template('signupMember.html')
    else:
        pt = PT.getAllPaymentType()
        return render_template('membership.html', pt=pt)

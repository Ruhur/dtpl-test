from app import app
from flask import render_template, request, redirect, url_for, Flask, session
# from app.models.user import User ## import kelas User dari model
import app.models.DB.User as DBuser
from hashlib import blake2b
import app.models.Komunitas as Komuni
from app.helper import helper


SECRET_KEY = b'DTPL AMAN BERSIH ANTI KORUPSI'
AUTH_SIZE = 16


@app.route('/signupMember', methods=["POST", "GET"])
def signupMember():

    # user =  User() ## membuat objek dari kelas user
    # nama = user.getName() ## memanggil method untuk mengambil nama

    print(request.form)
    print("callled---------------------------------")
    if request.method == "POST":
        print('post')
        name = request.form["name"]
        email = request.form["email"]
        password = request.form["password"]
        role = "member"
        password_confirmation = request.form["confirmpassword"]
        check_is_exist = DBuser.isUserExist(email)
        print(((password == password_confirmation)))
        if ((not check_is_exist) and (password == password_confirmation)):
            encrypt_pass = blake2b(digest_size=AUTH_SIZE, key=SECRET_KEY)
            encrypt_pass.update(password.encode())
            print(encrypt_pass.hexdigest().encode('utf-8'))
            DBuser.addUser(email, name, password, role)
        else:
            return render_template('signupMember.html')
        # CHECK DB | check weather its admin or not

        # IF NOT
        # return render_template('login.html')

        # IF GRANTED
        # return render_template('login.html')
        return redirect(url_for('login'))
        # return render_template('signupMember.html')
    else:
        return render_template('signupMember.html')


@app.route('/signupCommunity', methods=["POST", "GET"])
def signupCommunity():
    if request.method == "POST":
        helper.log_debug("post signup comunity")
        name = request.form["name"]
        email = request.form["email"]
        password = request.form["password"]
        password_confirmation = request.form["confirmpassword"]
        role = "community"
        check_is_exist = DBuser.isUserExist(email)
        print(((password == password_confirmation)))
        if ((not check_is_exist) and (password == password_confirmation)):
            DBuser.addUser(email, name, password, role)
        else:
            helper.log_info("Password is not matched!")
            return render_template('signupCommunity.html')

        return redirect(url_for('signupCommunityDetail'))
        # return render_template('signupMember.html')
    else:
        return render_template('signupCommunity.html')


@app.route('/signupCommunityDetail', methods=["POST", "GET"])
def signupCommunityDetail():
    if request.method == "POST":
        name = request.form["name"]
        desc = request.form["desc"]
        city = request.form["location"]
        helper.log_debug('create community: '+name)
        Komuni.createKomunitas(name, desc, city)

        session["comunityName"] = name
        # CHECK DB | check weather its admin or not

        # IF NOT
        # return render_template('login.html')

        # IF GRANTED
        # return render_template('login.html')

        return redirect(url_for('login'))
        # return render_template('signupMember.html')
    else:
        return render_template('signupCommunity_2.html')


@app.route('/<email>/<name>/page_template')
def page_template(email, name):
    print("called----------------------------------")
    return f"<h1>welcome email : {email}</h1> <br> name : {name}"

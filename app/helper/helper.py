from flask import flash
from app import app


def log_info(msg='Default info'):
    app.logger.info(msg)
    flash(msg, "flash_info")


def log_debug(msg='Defatul message'):
    app.logger.debug(msg)

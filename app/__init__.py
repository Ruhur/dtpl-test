from flask import Flask, flash, redirect, url_for
from flask_login import LoginManager
import logging

from app.models.DB.User import getUserByEmail  # import Flask dari package flask

lm = LoginManager()

app = Flask(__name__)

logging.basicConfig(filename='log.log', level=logging.DEBUG, format='%(asctime)s %(levelname)s %(name)s : %(message)s')
lm.init_app(app)

@lm.user_loader
def load_user(user_id):
    """Check if user is logged-in upon page load."""
    if user_id is not None:
        return getUserByEmail(user_id)
        # return User.query.get(user_id)
    return None


@lm.unauthorized_handler
def unauthorized():
    """Redirect unauthorized users to Login page."""
    flash("You must be logged in to view that page.")
    return redirect(url_for("auth_bp.login"))



from app.controllers import *

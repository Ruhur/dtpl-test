from app.models.DB.Connect import connectToDB
import psycopg2
import psycopg2.extras
from psycopg2 import sql
import datetime


def getAllPaymentType():
    '''
    get all payment type
    '''
    connect = connectToDB()
    cursor = connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    query = "select * from tref_payment_type"
    try:
        sql_string = sql.SQL(query)
        cursor.execute(sql_string)
        output = cursor.fetchall()
        return output

    except Exception as e:
        print(e)
        return 'fail'

from flask_login import UserMixin


class User(UserMixin):
    def __init__(self, email, name, role, passsword, active=True):
        self.id = email
        self.name = name
        self.password = passsword
        self.role = role
        self.active = active

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def get_name(self):
        return str(self.name)

    def get_role(self):
        return str(self.role)

    def check_password(self, pwd):
        if self.password == pwd:
            return True
        return False

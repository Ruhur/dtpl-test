from app.models.DB.Connect import connectToDB
import psycopg2
import psycopg2.extras
from psycopg2 import sql
import datetime


def getAllKomunitas():
    '''
    get all komunitas
    '''
    connect = connectToDB()
    cursor = connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    query = "select * from tmst_komunitas"
    try:
        sql_string = sql.SQL(query)
        cursor.execute(sql_string)
        output = cursor.fetchall()
        return output

    except Exception as e:
        print(e)
        return 'fail'


def getKomunitasByName(name):
    '''
    get komunitas by name like
    '''
    connect = connectToDB()
    cursor = connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try:
        cursor.execute(
            "select * from tmst_komunitas where is_active = true and name ilike %(name)s ", {'name': "%"+name+"%"})
        output = cursor.fetchall()
        return output

    except Exception as e:
        print(e)
        return 'fail'


def getDetailKomunitas(id):
    '''
    get detail komunitas
    '''
    connect = connectToDB()
    cursor = connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try:
        cursor.execute(
            "select * from tmst_komunitas where is_active = true and id = %(id)s ", {'id': id})
        output = cursor.fetchone()
        return output

    except Exception as e:
        print(e)
        return 'fail'


def isMember(idUser, idKomunitas):
    '''
    return boolean is user member
    '''
    connect = connectToDB()
    cursor = connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try:
        cursor.execute("select count(1) from tran_users_komunitas where id_user = %(idUser)s AND id_komunitas = %(idKomunitas)s", {
                       'idUser': idUser, 'idKomunitas': idKomunitas})
        output = cursor.fetchone()
        return output['count'] > 0

    except Exception as e:
        print(e)
        return 'fail'


def joinKomunitas(idUser, idKomunitas):
    '''
    Join Komunitas
    '''
    connect = connectToDB()
    cursor = connect.cursor()
    try:
        cursor.execute(
            "INSERT INTO tran_users_komunitas (id_user,  id_komunitas,  is_approve) VALUES (%(idUser)s,  %(idKomunitas)s,  %(is_approve)s)",
            {
                'idUser': idUser,
                'idKomunitas': idKomunitas,
                'is_approve': False
            }
        )
        connect.commit()
        cursor.close()
        return 'created'
    except Exception as e:
        print(e)
        print("asd")
        return 'fail'


def updateKomunitas(idKomunitas, description):
    connect = connectToDB()
    cursor = connect.cursor()
    try:
        cursor.execute(
            "UPDATE tmst_komunitas " +
            "SET description = %(description)s WHERE id = %(idKomunitas)s",
            {
                'description': description,
                'idKomunitas': idKomunitas,
            }
        )
        connect.commit()
        cursor.close()
        return 'updated'
    except Exception as e:
        print(e)
        return 'fail'


def createKomunitas(name, desc, city):
    connect = connectToDB()
    cursor = connect.cursor()
    try:
        now = datetime.datetime.now()
        cursor.execute(
            "INSERT INTO  tmst_komunitas (name,  description, city, created_at, is_public, is_active) VALUES (%(name)s,  %(desc)s, %(city)s, %(now)s, false, true)",
            {
                'name': name,
                'desc': desc,
                'city': city,
                'now': now,
            }
        )
        connect.commit()
        cursor.close()
        return True
    except Exception as e:
        print(e)
        return False


def getMembers(idKomunitas):
    '''
    get all komunitas
    '''
    connect = connectToDB()
    cursor = connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    # query = "select * from tmst_users tu join tran_users_komunitas tuk ON tu.id = tuk.id_user where tuk.id_komunitas = %(idKomunitas)s"
    try:
        # sql_string = sql.SQL(query, {'idKomunitas':idKomunitas})
        cursor.execute("select * from tmst_users tu join tran_users_komunitas tuk ON tu.id = tuk.id_user where tuk.id_komunitas = %(idKomunitas)s", 
            {'idKomunitas':idKomunitas})
        output = cursor.fetchall()
        return output

    except Exception as e:
        print(e)
        return 'fail'

def getIdKomunitasByIdUser(idUser):
    connect = connectToDB()
    cursor = connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try:
        cursor.execute(
            "select * from tran_users_komunitas tuk where id_user = %(id)s ", {'id': idUser})
        output = cursor.fetchone()
        return output

    except Exception as e:
        print(e)
        return 'fail'
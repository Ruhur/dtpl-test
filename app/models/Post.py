from app.models.DB.Connect import connectToDB
import psycopg2
import psycopg2.extras
from psycopg2 import sql
import datetime

def getAllPost(idKomunitas):
    '''
    get all post in komunitas
    '''
    connect = connectToDB()
    cursor = connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try:
        cursor.execute(
            "select p.*, pk.*, u.name from tran_post p join tran_post_komunitas pk on p.id = pk.id_post join tmst_users u on pk.id_user = u.id where pk.id_komunitas = %(idKomunitas)s order by created_at desc", { 'idKomunitas':idKomunitas}
            )
        output = cursor.fetchall()
        return output

    except Exception as e:
        print(e)
        return 'fail'


def savePost(post, idKomunitas, idUser):
    '''
    Join Komunitas
    '''
    connect = connectToDB()
    cursor = connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try:
        now = datetime.datetime.now()
        cursor.execute(
            "INSERT INTO tran_post (post, created_at) VALUES (%(post)s,  %(created_at)s) RETURNING id",
            {
                'post': post,
                'created_at': now
            }
        )
        connect.commit()
        newPost = cursor.fetchone()
        print(newPost)
        cursor.execute(
            "INSERT INTO tran_post_komunitas (id_post, id_komunitas, id_user) VALUES (%(id_post)s,  %(id_komunitas)s, %(id_user)s)",
            {
                'id_post': newPost['id'],
                'id_komunitas': idKomunitas,
                'id_user' : idUser
            }
        )
        connect.commit()
        cursor.close()
        return 'created'
    except Exception as e:
        print(e)
        print("asd")
        return 'fail'
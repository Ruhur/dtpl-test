import app.models.DB.Connect as Connect
import psycopg2
import psycopg2.extras
from psycopg2 import sql
import datetime
from app.models.User import User
# from app.helper import helper


def getAllUser():
    '''
    get all active users
    '''
    connect = Connect.connectToDB()
    cursor = connect.cursor()
    query = "select id, email, name from tmst_users where is_active = true"
    try:
        sql_string = sql.SQL(query)
        cursor.execute(sql_string)
        output = cursor.fetchall()
        return output

    except Exception as e:
        print(e)
        return 'fail'


def getUserByEmail(email):
    '''
    get active user by email
    '''
    connect = Connect.connectToDB()
    cursor = connect.cursor()
    try:
        cursor.execute("select email, name, role, password from tmst_users where is_active = true and email = %(email)s", {
            'email': email})
        output = cursor.fetchone()
        tmp = User(output[0], output[1], output[2], output[3])
        return tmp

    except Exception as e:
        print(e)
        return False


def getUserIdByEmail(email):
    '''
    get active id user by email
    '''
    connect = Connect.connectToDB()
    cursor = connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try:
        cursor.execute("select id from tmst_users where is_active = true and email = %(email)s", {
            'email': email})
        output = cursor.fetchone()

        return output

    except Exception as e:
        print(e)
        return 'fail'


def isUserExist(email):
    """
    return true if pengguna is exist, return false if pengguna doesn't exist
    """
    connect = Connect.connectToDB()
    cursor = connect.cursor()
    try:
        cursor.execute("select count(1) from tmst_users where email = %(email)s", {
                       'email': email})
        output = cursor.fetchone()[0]
        return output == 1
    except Exception as e:
        print(e)
        return 'fail'


def addUser(email, name, password, role):
    """
    ADD new pengguna
    """
    connect = Connect.connectToDB()
    cursor = connect.cursor()
    try:
        if (not isUserExist(name)):
            now = datetime.datetime.now()
            cursor.execute(
                "INSERT INTO tmst_users (email,  name,  password, created_at, is_active, role) VALUES (%(email)s,  %(name)s,  %(password)s, %(now)s, %(is_active)s, %(role)s)",
                {
                    'email': email,
                    'name': name,
                    'password': password,
                    'now': now,
                    'is_active': True,
                    'role': role
                }
            )
            connect.commit()
            cursor.close()
            return 'created'
        else:
            return 'fail'
    except Exception as e:
        print(e)
        print("asd")
        return 'fail'


def updatePassword(email, password):
    """
    update user
    """
    connect = Connect.connectToDB()
    cursor = connect.cursor()
    try:
        if (isUserExist(email)):
            cursor.execute(
                "update tmst_users set password = (%(password)s) where email = (%(email)s)",
                {
                    'email': email,
                    'password': password,
                }
            )
            connect.commit()
            cursor.close()
            return True
        else:
            return False
    except Exception as e:
        # helper.log_debug(e)
        return False

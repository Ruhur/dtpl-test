# dtpl-test

# Getting started
## Clone Prject
 lakukan dengan command
 ``` git clone https://gitlab.com/Ruhur/dtpl-test.git ```

## Create Environment Variable
 1. Untuk Windows jalankan
 ```python -m venv env```

 2. Aktivasi env
 ```./env/Scripts/activate``` 
 atau 
 ``` env/Scripts/activate ```

## Install Requirement
 Jalankan perintah 
 ```pip install -r requirements.txt```

# Develop

 Pastikan Anda di branch dev, jika belum pindah branch dengan command 
 ```git checkout dev```

## Add Project
 Lakukan dengan menggunakan command
 ``` git add <directory ```
 atau
 ``` git add . ```
 untuk add semua perubahan

## Commit project
 Lakukan dengan menggunakan command
 ``` git commit -m " pesan commit " ```

## lakukan pull (opsional)
 langkah ini opsional untuk mengupdate local dengan yang di push
 ``` git pull ```

## Lakukan push ke branch dev
  lakukan dengan commad
  ``` git push origin dev ```

## Membuat merge request
  buat permohonan merge ke main, dengan assignee @Ruhur / Winayaka Ruhur

## kabarin aja di wa abis itu.. wkwkwkwk